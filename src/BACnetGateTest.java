/*
 * File: BACnetGateTest.java
 * Project: org.unirostock.bagateway.bacnet
 * @author Robert Balla
 */


import org.unirostock.bagateway.bacnet.BACnetGateImpl;
import org.unirostock.bagateway.entitiy.RootEntity;
import org.unirostock.bagateway.interfaces.GateInterface;

// TODO: Auto-generated Javadoc
/**
 * The Class BACnetGateTest.
 */
public class BACnetGateTest
{
	
	/** The gate. */
	static GateInterface gate;
	
	/**
	 * The main method.
	 *
	 * @param args the arguments
	 * @throws InterruptedException the interrupted exception
	 */
	public static void main(String [ ] args) throws InterruptedException
	{
		gate = new BACnetGateImpl();
		gate.openGate();
		
		gate.discoverDevice(null);
//		Thread.sleep(2000);
//		gate.discoverDevice("BACnet4J slave device test");

//		RootEntity ent = new RootEntity("DPWS", "test", null);
//		gate.newDevice(ent);
	}
}
