/*
 * File: Activator.java
 * Project: org.unirostock.bagateway.bacnet
 * @author Robert Balla
 */
package org.unirostock.bagateway.bacnet;

import java.util.logging.Logger;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;
import org.osgi.framework.ServiceRegistration;
import org.unirostock.bagateway.interfaces.GateInterface;
import org.unirostock.bagateway.interfaces.GatewayInterface;


// TODO: Auto-generated Javadoc
/**
 * The Class Activator.
 */
public class Activator implements BundleActivator {

	/** The Constant log. */
	private static final Logger log = Logger.getLogger(BACnetGateImpl.class.getName() + "(Activator)");
	
	/** The context. */
	private static BundleContext context;
	
	/** The gate reg. */
	private ServiceRegistration<?> gateReg;
	
	/** The bacnet gate. */
	private BACnetGateImpl bacnetGate;
	
	/** The gateway ref. */
	private ServiceReference<GatewayInterface> gatewayRef;

	/**
	 * Gets the context.
	 *
	 * @return the context
	 */
	static BundleContext getContext() {
		return context;
	}

	/*
	 * (non-Javadoc)
	 * @see org.osgi.framework.BundleActivator#start(org.osgi.framework.BundleContext)
	 */
	public void start(BundleContext bundleContext) throws Exception
	{
		log.info("Start BACnetGate-Bundle");
		
		Activator.context = bundleContext;
		
		gateReg = context.registerService(GateInterface.class.getName(), new BACnetGateImpl(), null);
		gatewayRef = context.getServiceReference(GatewayInterface.class);
		bacnetGate = (BACnetGateImpl) context.getService(gateReg.getReference());
		bacnetGate.setGateway(context.getService(gatewayRef));
	}

	/*
	 * (non-Javadoc)
	 * @see org.osgi.framework.BundleActivator#stop(org.osgi.framework.BundleContext)
	 */
	public void stop(BundleContext bundleContext) throws Exception
	{
		log.info("Stop BACnetGate-Bundle");
		
		bacnetGate.closeGate();
		context.ungetService(gatewayRef);
		gateReg.unregister();
		Activator.context = null;
	}

}

