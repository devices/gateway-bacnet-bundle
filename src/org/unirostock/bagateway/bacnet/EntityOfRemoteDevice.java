/*
 * File: EntityOfDevice.java
 * Project: org.unirostock.bagateway.bacnet
 * @author Robert Balla
 */
package org.unirostock.bagateway.bacnet;

import java.util.List;

import org.unirostock.bagateway.entitiy.RootEntity;

import com.serotonin.bacnet4j.RemoteDevice;
import com.serotonin.bacnet4j.RemoteObject;
import com.serotonin.bacnet4j.exception.BACnetException;
import com.serotonin.bacnet4j.type.enumerated.ObjectType;
import com.serotonin.bacnet4j.type.enumerated.PropertyIdentifier;
import com.serotonin.bacnet4j.type.primitive.ObjectIdentifier;
import com.serotonin.bacnet4j.util.PropertyReferences;
import com.serotonin.bacnet4j.util.PropertyValues;
import com.serotonin.bacnet4j.util.RequestUtils;

// TODO: Auto-generated Javadoc
/**
 * The Class EntityOfDevice.
 */
public class EntityOfRemoteDevice extends RootEntity
{

	/**
	 * Instantiates a new entity of device.
	 *
	 * @param protocol the protocol
	 * @param remD the remote device
	 * @param id the id
	 * @param objects the objects
	 * @param gateDevice the gate device
	 * @throws BACnetException the BACnet exception
	 */
	public EntityOfRemoteDevice(String protocol, RemoteDevice remD, Object id, List<ObjectIdentifier> objects, GateDevice gateDevice) throws BACnetException
	{
		super(protocol, remD.getName(), id);
		
		PropertyReferences propRefsName = new PropertyReferences();
		for (ObjectIdentifier objectId : objects)
		{
			remD.setObject(new RemoteObject(objectId));
			propRefsName.add(objectId, PropertyIdentifier.objectName);
		}
		PropertyValues objNames = RequestUtils.readProperties(gateDevice, remD, propRefsName, null);
     		  
		RemoteObject remO;

		for (ObjectIdentifier objectId : objects)
		{
//			  if (!SupportedObjectTypes.types.contains(objectId.getObjectType()))
//					continue;
	  
			remO = remD.getObject(objectId);
			remO.setObjectName(objNames.getString(objectId, PropertyIdentifier.objectName));
	  
			this.addSubEntity(new EntityOfRemoteObject(gateDevice, remD, remO, this.getPath()));   			  
		}	
	}
}
