package org.unirostock.bagateway.bacnet;

import java.util.LinkedList;

import org.unirostock.bagateway.bacnet.tables.ParameterTable;
import org.unirostock.bagateway.entitiy.GatewayInternalEntity;
import org.unirostock.bagateway.entitiy.ParameterEntity;

import com.serotonin.bacnet4j.type.enumerated.PropertyIdentifier;

public class ForeignProperty
{
	private PropertyIdentifier propId;
	private LinkedList<Object> path;
	private boolean readable;
	private boolean writeable;
	private boolean subscribable;
	
	public ForeignProperty (ParameterEntity param)
	{
		this.propId = ParameterTable.gw2bac.get(param.getParameterName());
		if (this.propId == null)
			propId = PropertyIdentifier.presentValue;
		this.path = param.getPath();
		this.readable = param.isReadable();
		this.writeable = param.isWriteable();
		this.subscribable = param.isSubscribable();
	}
	
	public ForeignProperty (PropertyIdentifier propId)
	{
		this.propId = propId;
		this.path = null;
		this.readable = false;
		this.writeable = false;
		this.subscribable = false;
	}
	
	public PropertyIdentifier getPropertyIdentifier()
	{
		return this.propId;
	}
	
	public LinkedList<Object> getPath()
	{
		return path;
	}
	
	public void setPath(LinkedList<Object> p)
	{
		this.path = p;
	}
	
	public boolean isReadable()
	{
		return this.readable;
	}
	
	public void setReadable(boolean bool)
	{
		this.readable = bool;
	}
	
	public boolean isWriteable()
	{
		return this.writeable;
	}
	
	public void setWriteable(boolean bool)
	{
		this.writeable = bool;
	}
	
	public boolean isSubscribable()
	{
		return this.subscribable;
	}
	
	public void setSubscribable(boolean bool)
	{
		this.subscribable = bool;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals (Object o)
	{
		boolean retVal = false;
		
		if (o instanceof ForeignProperty)
		{		
			ForeignProperty obj = (ForeignProperty) o;
			
			if (this.propId.equals(obj.propId))
			{
				retVal = true;
			}
		}
		
		return retVal;
	}
}
