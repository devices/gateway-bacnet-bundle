/*
 * File: DatatypeTable.java
 * Project: org.unirostock.bagateway.bacnet
 * @author Robert Balla
 */
package org.unirostock.bagateway.bacnet.tables;

import java.util.HashMap;

import org.unirostock.bagateway.enums.GWdatatype;

import com.serotonin.bacnet4j.type.Encodable;
import com.serotonin.bacnet4j.type.primitive.BitString;
import com.serotonin.bacnet4j.type.primitive.Boolean;
import com.serotonin.bacnet4j.type.primitive.CharacterString;
import com.serotonin.bacnet4j.type.primitive.Double;
import com.serotonin.bacnet4j.type.primitive.OctetString;
import com.serotonin.bacnet4j.type.primitive.Real;
import com.serotonin.bacnet4j.type.primitive.SignedInteger;
import com.serotonin.bacnet4j.type.primitive.UnsignedInteger;
import com.serotonin.bacnet4j.type.enumerated.BinaryPV;
import com.serotonin.bacnet4j.type.enumerated.EngineeringUnits;

public class DatatypeTable
{
	public static final HashMap<Class<? extends Encodable>, GWdatatype> bac2gw;
	public static final HashMap<GWdatatype, Class<? extends Encodable>> gw2bac;
	
	static
	{
		bac2gw = new HashMap<Class<? extends Encodable>, GWdatatype>();
		gw2bac = new HashMap<GWdatatype, Class<? extends Encodable>>();
		
		bac2gw.put(Boolean.class, GWdatatype.Boolean);
		bac2gw.put(BinaryPV.class, GWdatatype.Boolean);
		bac2gw.put(CharacterString.class, GWdatatype.CharacterString);
		bac2gw.put(Double.class, GWdatatype.Double);
		bac2gw.put(EngineeringUnits.class, GWdatatype.Unit);
		bac2gw.put(Real.class, GWdatatype.Float);
		bac2gw.put(UnsignedInteger.class, GWdatatype.UnsignedInteger);
		bac2gw.put(SignedInteger.class, GWdatatype.Integer);
		
		gw2bac.put(GWdatatype.Boolean, Boolean.class);
		gw2bac.put(GWdatatype.Byte, OctetString.class);
		gw2bac.put(GWdatatype.CharacterString, CharacterString.class);
		gw2bac.put(GWdatatype.Double, Double.class);
		gw2bac.put(GWdatatype.Float, Real.class);
		gw2bac.put(GWdatatype.Integer, SignedInteger.class);
		gw2bac.put(GWdatatype.Unit, EngineeringUnits.class);	
		gw2bac.put(GWdatatype.UnsignedInteger, UnsignedInteger.class);		
	}
}
