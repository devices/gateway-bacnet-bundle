/*
 * File: PropertyTable.java
 * Project: org.unirostock.bagateway.bacnet
 * @author Robert Balla
 */
package org.unirostock.bagateway.bacnet.tables;

import java.util.HashMap;

import org.unirostock.bagateway.enums.GWparameter;

import com.serotonin.bacnet4j.type.enumerated.PropertyIdentifier;

public class ParameterTable
{
	public static final HashMap<PropertyIdentifier, GWparameter> bac2gw;
	public static final HashMap<GWparameter, PropertyIdentifier> gw2bac;
	
	static
	{
		bac2gw = new HashMap<PropertyIdentifier, GWparameter>();
		gw2bac = new HashMap<GWparameter, PropertyIdentifier>();
		
		bac2gw.put(PropertyIdentifier.activeText,		GWparameter.INFO_ACTIVE);
		bac2gw.put(PropertyIdentifier.covIncrement,	GWparameter.NOTIFICATION_INCREMENT);
		bac2gw.put(PropertyIdentifier.description,	GWparameter.INFO);
		bac2gw.put(PropertyIdentifier.highLimit,		GWparameter.NOTIFICATION_LIMIT_MAX);
		bac2gw.put(PropertyIdentifier.inactiveText,	GWparameter.INFO_INACTIVE);
		bac2gw.put(PropertyIdentifier.limitEnable,	GWparameter.NOTIFICATION_LIMITS_ON);
		bac2gw.put(PropertyIdentifier.lowLimit,		GWparameter.NOTIFICATION_LIMIT_MIN);
		bac2gw.put(PropertyIdentifier.maxPresValue,	GWparameter.PRESVAL_LIMIT_MAX);
		bac2gw.put(PropertyIdentifier.minPresValue,	GWparameter.PRESVAL_LIMIT_MIN);
		bac2gw.put(PropertyIdentifier.numberOfStates,GWparameter.PRESVAL_STATES_NUM);
		bac2gw.put(PropertyIdentifier.objectName,		GWparameter.NAME);
		bac2gw.put(PropertyIdentifier.polarity,		GWparameter.PRESVAL_POLARITY);
		bac2gw.put(PropertyIdentifier.presentValue,	GWparameter.PRESENT_VALUE);
		bac2gw.put(PropertyIdentifier.resolution,		GWparameter.PRESVAL_RESOLUTION);
		bac2gw.put(PropertyIdentifier.stateText,		GWparameter.PRESVAL_STATES_INFO);
		bac2gw.put(PropertyIdentifier.statusFlags,	GWparameter.STATUS);
		bac2gw.put(PropertyIdentifier.timeDelay,		GWparameter.NOTIFICATION_DELAY);
		bac2gw.put(PropertyIdentifier.units,			GWparameter.PRESVAL_UNIT);
		bac2gw.put(PropertyIdentifier.updateInterval,GWparameter.PRESVAL_UPDATEINTERVAL);

		gw2bac.put(GWparameter.INFO_ACTIVE,					PropertyIdentifier.activeText);
		gw2bac.put(GWparameter.NOTIFICATION_INCREMENT,	PropertyIdentifier.covIncrement);
		gw2bac.put(GWparameter.INFO,							PropertyIdentifier.description);
		gw2bac.put(GWparameter.NOTIFICATION_LIMIT_MAX,	PropertyIdentifier.highLimit);
		gw2bac.put(GWparameter.INFO_INACTIVE,				PropertyIdentifier.inactiveText);
		gw2bac.put(GWparameter.NOTIFICATION_LIMITS_ON,	PropertyIdentifier.limitEnable);
		gw2bac.put(GWparameter.NOTIFICATION_LIMIT_MIN,	PropertyIdentifier.lowLimit);
		gw2bac.put(GWparameter.PRESVAL_LIMIT_MAX,			PropertyIdentifier.maxPresValue);
		gw2bac.put(GWparameter.PRESVAL_LIMIT_MIN,			PropertyIdentifier.minPresValue);
		gw2bac.put(GWparameter.PRESVAL_STATES_NUM,		PropertyIdentifier.numberOfStates);
		gw2bac.put(GWparameter.NAME,							PropertyIdentifier.objectName);
		gw2bac.put(GWparameter.PRESVAL_POLARITY,			PropertyIdentifier.polarity);
		gw2bac.put(GWparameter.PRESENT_VALUE,				PropertyIdentifier.presentValue);
		gw2bac.put(GWparameter.PRESVAL_RESOLUTION,		PropertyIdentifier.resolution);
		gw2bac.put(GWparameter.PRESVAL_STATES_INFO,		PropertyIdentifier.stateText);
		gw2bac.put(GWparameter.STATUS,						PropertyIdentifier.statusFlags);
		gw2bac.put(GWparameter.NOTIFICATION_DELAY,		PropertyIdentifier.timeDelay);
		gw2bac.put(GWparameter.PRESVAL_UNIT,				PropertyIdentifier.units);
		gw2bac.put(GWparameter.PRESVAL_UPDATEINTERVAL,	PropertyIdentifier.updateInterval);
	}
}
