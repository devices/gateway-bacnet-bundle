package org.unirostock.bagateway.bacnet.tables;

import java.util.LinkedList;

import com.serotonin.bacnet4j.type.enumerated.ObjectType;

public class SupportedObjectTypes
{
	public static LinkedList<ObjectType> types = new LinkedList<ObjectType>();
	
	static
	{	
		types.add(ObjectType.analogInput);
		types.add(ObjectType.analogOutput);
		types.add(ObjectType.analogValue);
		
		types.add(ObjectType.binaryInput);
		types.add(ObjectType.binaryOutput);
		types.add(ObjectType.binaryValue);
		
		types.add(ObjectType.multiStateInput);
		types.add(ObjectType.multiStateOutput);
		types.add(ObjectType.multiStateValue);
	}
}