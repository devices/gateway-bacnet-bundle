/*
 * File: ItemTable.java
 * Project: org.unirostock.bagateway.bacnet
 * @author Robert Balla
 */
package org.unirostock.bagateway.bacnet.tables;

import java.util.HashMap;

import org.unirostock.bagateway.enums.GWgroup;

import com.serotonin.bacnet4j.type.enumerated.EngineeringUnits;

public class GroupTable
{
	public static final HashMap<EngineeringUnits, GWgroup> bac2gw;
	//public static final HashMap<GatewayInternalPhysicalProperty, EngineeringUnits> gw2bac;
	
	static
	{
		bac2gw = new HashMap<EngineeringUnits, GWgroup>();
		//gw2bac = new HashMap<GatewayInternalPhysicalProperty, EngineeringUnits>();
		
		bac2gw.put(EngineeringUnits.metersPerSecondPerSecond,			GWgroup.ACCELERATION);
		bac2gw.put(EngineeringUnits.squareCentimeters,					GWgroup.AREA);
		bac2gw.put(EngineeringUnits.squareFeet,							GWgroup.AREA);
		bac2gw.put(EngineeringUnits.squareInches,							GWgroup.AREA);
		bac2gw.put(EngineeringUnits.squareMeters,							GWgroup.AREA);
		bac2gw.put(EngineeringUnits.farads,									GWgroup.CAPACITANCE);
		bac2gw.put(EngineeringUnits.siemens,								GWgroup.CONDUCTANCE);
		bac2gw.put(EngineeringUnits.amperes,								GWgroup.CURRENT);
		bac2gw.put(EngineeringUnits.milliamperes,							GWgroup.CURRENT);
		bac2gw.put(EngineeringUnits.joules,									GWgroup.ENERGY);
		bac2gw.put(EngineeringUnits.kilojoules,							GWgroup.ENERGY);
		bac2gw.put(EngineeringUnits.megajoules,							GWgroup.ENERGY);
		bac2gw.put(EngineeringUnits.newton,									GWgroup.FORCE);
		bac2gw.put(EngineeringUnits.cyclesPerHour,						GWgroup.FREQUENCY);
		bac2gw.put(EngineeringUnits.cyclesPerMinute,						GWgroup.FREQUENCY);
		bac2gw.put(EngineeringUnits.hertz,									GWgroup.FREQUENCY);
		bac2gw.put(EngineeringUnits.kilohertz,								GWgroup.FREQUENCY);
		bac2gw.put(EngineeringUnits.megahertz,								GWgroup.FREQUENCY);
		bac2gw.put(EngineeringUnits.perHour,								GWgroup.FREQUENCY);
		bac2gw.put(EngineeringUnits.perMinute,								GWgroup.FREQUENCY);
		bac2gw.put(EngineeringUnits.perSecond,								GWgroup.FREQUENCY);
		bac2gw.put(EngineeringUnits.gramsOfWaterPerKilogramDryAir,	GWgroup.HUMIDITY);
		bac2gw.put(EngineeringUnits.percentRelativeHumidity,			GWgroup.HUMIDITY);
		bac2gw.put(EngineeringUnits.luxes,									GWgroup.ILLUMINATION);
		bac2gw.put(EngineeringUnits.henrys,									GWgroup.INDUCTANCE);
		bac2gw.put(EngineeringUnits.meters,									GWgroup.LENGTH);
		bac2gw.put(EngineeringUnits.millimeters,							GWgroup.LENGTH);
		bac2gw.put(EngineeringUnits.centimeters,							GWgroup.LENGTH);
		bac2gw.put(EngineeringUnits.inches,									GWgroup.LENGTH);
		bac2gw.put(EngineeringUnits.feet,									GWgroup.LENGTH);
		bac2gw.put(EngineeringUnits.lumens,									GWgroup.LUMINOUS_FLUX);
		bac2gw.put(EngineeringUnits.candelas,								GWgroup.LUMINOUS_INTENSITY);
		bac2gw.put(EngineeringUnits.kilograms,								GWgroup.MASS);
		bac2gw.put(EngineeringUnits.poundsMass,							GWgroup.MASS);
		bac2gw.put(EngineeringUnits.tons,									GWgroup.MASS);
		bac2gw.put(EngineeringUnits.watts,									GWgroup.POWER);
		bac2gw.put(EngineeringUnits.milliwatts,							GWgroup.POWER);
		bac2gw.put(EngineeringUnits.kilowatts,								GWgroup.POWER);
		bac2gw.put(EngineeringUnits.megawatts,								GWgroup.POWER);
		bac2gw.put(EngineeringUnits.horsepower,							GWgroup.POWER);
		bac2gw.put(EngineeringUnits.pascals,								GWgroup.PRESSURE);
		bac2gw.put(EngineeringUnits.hectopascals,							GWgroup.PRESSURE);
		bac2gw.put(EngineeringUnits.kilopascals,							GWgroup.PRESSURE);
		bac2gw.put(EngineeringUnits.bars,									GWgroup.PRESSURE);
		bac2gw.put(EngineeringUnits.millibars,								GWgroup.PRESSURE);
		bac2gw.put(EngineeringUnits.ohms,									GWgroup.RESISTANCE);
		bac2gw.put(EngineeringUnits.milliohms,								GWgroup.RESISTANCE);
		bac2gw.put(EngineeringUnits.kilohms,								GWgroup.RESISTANCE);
		bac2gw.put(EngineeringUnits.megohms,								GWgroup.RESISTANCE);
		bac2gw.put(EngineeringUnits.degreesCelsius,						GWgroup.TEMPERATURE);
		bac2gw.put(EngineeringUnits.degreesKelvin,						GWgroup.TEMPERATURE);
		bac2gw.put(EngineeringUnits.degreesFahrenheit,					GWgroup.TEMPERATURE);
		bac2gw.put(EngineeringUnits.years,									GWgroup.TIME);
		bac2gw.put(EngineeringUnits.months,									GWgroup.TIME);
		bac2gw.put(EngineeringUnits.weeks,									GWgroup.TIME);
		bac2gw.put(EngineeringUnits.days,									GWgroup.TIME);
		bac2gw.put(EngineeringUnits.hours,									GWgroup.TIME);
		bac2gw.put(EngineeringUnits.minutes,								GWgroup.TIME);
		bac2gw.put(EngineeringUnits.seconds,								GWgroup.TIME);
		bac2gw.put(EngineeringUnits.hundredthsSeconds,					GWgroup.TIME);
		bac2gw.put(EngineeringUnits.milliseconds,							GWgroup.TIME);
		bac2gw.put(EngineeringUnits.millimetersPerSecond,				GWgroup.VELOCITY);
		bac2gw.put(EngineeringUnits.millimetersPerMinute,				GWgroup.VELOCITY);
		bac2gw.put(EngineeringUnits.metersPerSecond,						GWgroup.VELOCITY);
		bac2gw.put(EngineeringUnits.metersPerMinute,						GWgroup.VELOCITY);
		bac2gw.put(EngineeringUnits.metersPerHour,						GWgroup.VELOCITY);
		bac2gw.put(EngineeringUnits.kilometersPerHour,					GWgroup.VELOCITY);
		bac2gw.put(EngineeringUnits.feetPerSecond,						GWgroup.VELOCITY);
		bac2gw.put(EngineeringUnits.feetPerMinute,						GWgroup.VELOCITY);
		bac2gw.put(EngineeringUnits.milesPerHour,							GWgroup.VELOCITY);
		bac2gw.put(EngineeringUnits.volts,									GWgroup.VOLTAGE);
		bac2gw.put(EngineeringUnits.millivolts,							GWgroup.VOLTAGE);
		bac2gw.put(EngineeringUnits.kilovolts,								GWgroup.VOLTAGE);
		bac2gw.put(EngineeringUnits.megavolts,								GWgroup.VOLTAGE);		
		bac2gw.put(EngineeringUnits.cubicFeet,								GWgroup.VOLUME);
		bac2gw.put(EngineeringUnits.cubicMeters,							GWgroup.VOLUME);
		bac2gw.put(EngineeringUnits.imperialGallons,						GWgroup.VOLUME);
		bac2gw.put(EngineeringUnits.liters,									GWgroup.VOLUME);
		bac2gw.put(EngineeringUnits.usGallons,								GWgroup.VOLUME);
	}
}
