package org.unirostock.bagateway.bacnet.tables;

import java.util.HashMap;

import org.unirostock.bagateway.enums.GWunit;

import com.serotonin.bacnet4j.type.enumerated.EngineeringUnits;

public class UnitTable
{
	public static final HashMap<EngineeringUnits, GWunit> bac2gw;
	public static final HashMap<GWunit, EngineeringUnits> gw2bac;
	
	static
	{
		bac2gw = new HashMap<EngineeringUnits, GWunit>();
		gw2bac = new HashMap<GWunit, EngineeringUnits>();
		
		bac2gw.put(EngineeringUnits.percent, GWunit.Percent);
		bac2gw.put(EngineeringUnits.degreesCelsius, GWunit.Celsius);
		bac2gw.put(EngineeringUnits.degreesFahrenheit, GWunit.Fahrenheit);
		bac2gw.put(EngineeringUnits.degreesKelvin, GWunit.Kelvin);
		
		gw2bac.put(GWunit.Percent, EngineeringUnits.percent);
		gw2bac.put(GWunit.Celsius, EngineeringUnits.degreesCelsius);
		gw2bac.put(GWunit.Fahrenheit, EngineeringUnits.degreesFahrenheit);
		gw2bac.put(GWunit.Kelvin, EngineeringUnits.degreesKelvin);
	}
}
