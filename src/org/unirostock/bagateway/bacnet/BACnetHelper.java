package org.unirostock.bagateway.bacnet;

import com.serotonin.bacnet4j.type.Encodable;
import com.serotonin.bacnet4j.type.enumerated.BinaryPV;
import com.serotonin.bacnet4j.type.primitive.Boolean;
import com.serotonin.bacnet4j.type.primitive.CharacterString;
import com.serotonin.bacnet4j.type.primitive.Real;
import com.serotonin.bacnet4j.type.primitive.SignedInteger;
import com.serotonin.bacnet4j.type.primitive.UnsignedInteger;


public class BACnetHelper
{
	static Encodable convertValueToEncodable(String val, Class<? extends Encodable> typeId)
	{
		Encodable value = null;		
		if (typeId.equals(Real.class))
		{
			value = new Real(Float.parseFloat(val));
		}else if (typeId.equals(UnsignedInteger.class))
		{
			value = new UnsignedInteger(Integer.parseInt(val));
		}else if(typeId.equals(SignedInteger.class))
		{
			value = new SignedInteger(Integer.parseInt(val));
		}else if (typeId.equals(Boolean.class))
		{
			if (val.equals("true") || val.equals("1"))
			{
				value = new Boolean(true);
			}else if( val.equals("false") || val.equals("0"))
			{
				value = new Boolean(false);
			}
			
		}else if (typeId.equals(BinaryPV.class))
		{
			if (val.equals("true") || val.equals("1"))
			{
				value = BinaryPV.active;
			}else if( val.equals("false") || val.equals("0"))
			{
				value = BinaryPV.inactive;
			}
		}else
		{
			value = new CharacterString(val);
		}
		
		return value;
	}
}
