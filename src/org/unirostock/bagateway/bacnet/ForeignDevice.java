/*
 * File: ForeignDevice.java
 * Project: org.unirostock.bagateway.bacnet
 * @author Robert Balla
 */
package org.unirostock.bagateway.bacnet;

import java.util.LinkedList;
import java.util.logging.Logger;

import org.unirostock.bagateway.bacnet.BACnetGateImpl.BACnetListener;
import org.unirostock.bagateway.bacnet.tables.ParameterTable;
import org.unirostock.bagateway.entitiy.GatewayInternalEntity;
import org.unirostock.bagateway.entitiy.ParameterEntity;
import org.unirostock.bagateway.entitiy.RootEntity;
import org.unirostock.bagateway.entitiy.SubEntity;
import org.unirostock.bagateway.enums.GWdatatype;
import org.unirostock.bagateway.enums.GWparameter;
import org.unirostock.bagateway.interfaces.GatewayInterface;

import com.serotonin.bacnet4j.LocalDevice;
import com.serotonin.bacnet4j.RemoteDevice;
import com.serotonin.bacnet4j.RemoteObject;
import com.serotonin.bacnet4j.event.DeviceEventListener;
import com.serotonin.bacnet4j.exception.BACnetException;
import com.serotonin.bacnet4j.exception.BACnetServiceException;
import com.serotonin.bacnet4j.npdu.ip.IpNetwork;
import com.serotonin.bacnet4j.obj.BACnetObject;
import com.serotonin.bacnet4j.obj.ObjectProperties;
import com.serotonin.bacnet4j.obj.PropertyTypeDefinition;
import com.serotonin.bacnet4j.service.confirmed.ReinitializeDeviceRequest.ReinitializedStateOfDevice;
import com.serotonin.bacnet4j.transport.Transport;
import com.serotonin.bacnet4j.type.Encodable;
import com.serotonin.bacnet4j.type.constructed.Address;
import com.serotonin.bacnet4j.type.constructed.Choice;
import com.serotonin.bacnet4j.type.constructed.DateTime;
import com.serotonin.bacnet4j.type.constructed.PropertyValue;
import com.serotonin.bacnet4j.type.constructed.SequenceOf;
import com.serotonin.bacnet4j.type.constructed.ServicesSupported;
import com.serotonin.bacnet4j.type.constructed.TimeStamp;
import com.serotonin.bacnet4j.type.enumerated.EventState;
import com.serotonin.bacnet4j.type.enumerated.EventType;
import com.serotonin.bacnet4j.type.enumerated.MessagePriority;
import com.serotonin.bacnet4j.type.enumerated.NotifyType;
import com.serotonin.bacnet4j.type.enumerated.ObjectType;
import com.serotonin.bacnet4j.type.enumerated.PropertyIdentifier;
import com.serotonin.bacnet4j.type.notificationParameters.NotificationParameters;
import com.serotonin.bacnet4j.type.primitive.BitString;
import com.serotonin.bacnet4j.type.primitive.Boolean;
import com.serotonin.bacnet4j.type.primitive.CharacterString;
import com.serotonin.bacnet4j.type.primitive.ObjectIdentifier;
import com.serotonin.bacnet4j.type.primitive.Primitive;
import com.serotonin.bacnet4j.type.primitive.UnsignedInteger;
import com.serotonin.util.queue.ByteQueue;


// TODO: Auto-generated Javadoc
/**
 * The Class ForeignDevice.
 */
public class ForeignDevice extends LocalDevice
{
	
	/** The Constant log. */
	private static final Logger log = Logger.getLogger(BACnetGateImpl.class.getName());
	
   /** The broadcast address. */
   final String broadcastAddress; 
   
	/** The gateway. */
	private GatewayInterface gateway;
	
	private LinkedList<Object> path;
	
	/**
	 * Instantiates a new foreign device.
	 *
	 * @param id the id
	 * @param transport the transport
	 * @param ent the ent
	 * @param broadcastAddress the broadcast address
	 * @param gateway the gateway
	 */
	public ForeignDevice(int id, Transport transport, RootEntity ent, String broadcastAddress, GatewayInterface gateway)
	{
		super(id, transport);

		this.broadcastAddress = broadcastAddress;
		this.gateway = gateway;
		this.path = ent.getPath();
		
		try
		{
			/*
			 * set devicename
			 */
			this.getConfiguration().setProperty(PropertyIdentifier.objectName, new CharacterString(ent.getName()));
			
			/*
			 * add objects
			 */
			for (SubEntity subEnt : ent.getAllSubEntities())
			{
				makeObjectOfEntity(subEnt, ent.getName());
			}
			
//			ServicesSupported servicesSup = this.getServicesSupported();
//			servicesSup.setSubscribeCov(false);
			
			/*
			 * initialize device
			 * add the eventlistener
			 * send IAm
			 */
			this.initialize();
			this.getEventHandler().addListener(new Listener());
			this.sendUnconfirmed(new Address(broadcastAddress, IpNetwork.DEFAULT_PORT), this.getIAm());
		} catch (Exception e)
		{
			log.warning("Error while making new device from Entity!");
			e.printStackTrace();
		}
	}
	
	public void sendIAm()
	{
		try
		{
			this.sendUnconfirmed(new Address(broadcastAddress, IpNetwork.DEFAULT_PORT), this.getIAm());
		} catch (BACnetException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * Make object of entity.
	 *
	 * @param ent the ent
	 * @throws BACnetServiceException 
	 */
	private void makeObjectOfEntity(SubEntity ent, String name) throws BACnetServiceException
	{
		LinkedList<ParameterEntity> parameterOrig = ent.getAllParameter();
		
		if (parameterOrig != null)
		{
			LinkedList<ParameterEntity> parameter = (LinkedList<ParameterEntity>)  parameterOrig.clone();
			ForeignObject obj = null;
			
			for(ParameterEntity param : parameter)
			{				
				if ((param.getParameterName() != null))
				{
					if(param.getParameterName().equals(GWparameter.PRESENT_VALUE))
					{
						ObjectType objType = getObjectType(param.getDatatype());
						obj = new ForeignObject(this, this.getNextInstanceObjectIdentifier(objType), ent, name + "/" + ent.getName(), this.gateway);
						obj.addProperty(new ForeignProperty(param));
						parameter.remove(param);
						break;
					}
				}
			}
			
			if ( obj != null)
			{
				for(ParameterEntity param : parameter)
				{				
					PropertyIdentifier propId = ParameterTable.gw2bac.get(param.getParameterName());
					PropertyTypeDefinition propDef = ObjectProperties.getPropertyTypeDefinition(obj.getId().getObjectType(), propId);
					if (propDef != null)
						obj.addProperty(new ForeignProperty(param));
					
					parameter.remove(param);
				}
				
				this.addObject(obj);
				obj.finalize();
			}
			
			for(ParameterEntity param : parameter)
			{				
				ObjectType objType = getObjectType(param.getDatatype());
				obj = new ForeignObject(this, this.getNextInstanceObjectIdentifier(objType), ent, name + "/" + ent.getName() + "/" + param.getName(), this.gateway);
				obj.addProperty(new ForeignProperty(param));
				this.addObject(obj);
				obj.finalize();
			}
		}
		
//		if ( ent.isReadable() || ent.isWriteable() || ent.isSubscribable())
//		{
//			ObjectType objType = getObjectType(ent.getDatatype());
//			
//			try
//			{
//				ForeignObject obj = new ForeignObject(this, this.getNextInstanceObjectIdentifier(objType), ent, name + "/" + ent.getName(), this.gateway);
//				this.addObject(obj);
//				obj.finalize();
//				
//			} catch (BACnetServiceException e)
//			{
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
//		}
		
		if (ent.getAllSubEntities() != null)
		{
			LinkedList<SubEntity> subEnts = ent.getAllSubEntities();
			for (SubEntity subEnt : subEnts)
			{
				makeObjectOfEntity(subEnt, name + "/" + ent.getName());
			}
		}

	}
	
	/**
	 * Gets the object type.
	 *
	 * @param type the type
	 * @return the object type
	 */
	private ObjectType getObjectType(GWdatatype type)
	{
		if (type == null)
			return ObjectType.characterStringValue;
		
		if (type.equals(GWdatatype.Boolean))
		{
			return ObjectType.binaryValue;
		}
		else if (type.equals(GWdatatype.Double))
		{
			return ObjectType.largeAnalogValue;
		}
		else if (type.equals(GWdatatype.Float))
		{
			return ObjectType.analogValue;
		}
		else if (type.equals(GWdatatype.Integer))
		{
			return ObjectType.integerValue;
		}
		else if (type.equals(GWdatatype.UnsignedInteger))
		{
			return ObjectType.positiveIntegerValue;
		}
		else if (type.equals(GWdatatype.Byte))
		{
			return ObjectType.octetStringValue;
		}
		else
		{
			return ObjectType.characterStringValue;
		}
	}
	
	/**
	 * The Class Listener.
	 */
	class Listener implements DeviceEventListener
	{

		/* (non-Javadoc)
		 * @see com.serotonin.bacnet4j.event.DeviceEventListener#listenerException(java.lang.Throwable)
		 */
		@Override
		public void listenerException(Throwable e)
		{
			// TODO Auto-generated method stub
			
		}

		/* (non-Javadoc)
		 * @see com.serotonin.bacnet4j.event.DeviceEventListener#iAmReceived(com.serotonin.bacnet4j.RemoteDevice)
		 */
		@Override
		public void iAmReceived(RemoteDevice d)
		{
			// TODO Auto-generated method stub
			
		}

		/* (non-Javadoc)
		 * @see com.serotonin.bacnet4j.event.DeviceEventListener#allowPropertyWrite(com.serotonin.bacnet4j.obj.BACnetObject, com.serotonin.bacnet4j.type.constructed.PropertyValue)
		 */
		@Override
		public boolean allowPropertyWrite(BACnetObject obj, PropertyValue pv)
		{
			ForeignObject object = (ForeignObject) ForeignDevice.this.getObject(obj.getObjectName());
			return object.isWriteable(pv.getPropertyIdentifier());
		}

		/* (non-Javadoc)
		 * @see com.serotonin.bacnet4j.event.DeviceEventListener#propertyWritten(com.serotonin.bacnet4j.obj.BACnetObject, com.serotonin.bacnet4j.type.constructed.PropertyValue)
		 */
		@Override
		public void propertyWritten(BACnetObject obj, PropertyValue pv)
		{
			// TODO Auto-generated method stub
			
		}

		/* (non-Javadoc)
		 * @see com.serotonin.bacnet4j.event.DeviceEventListener#iHaveReceived(com.serotonin.bacnet4j.RemoteDevice, com.serotonin.bacnet4j.RemoteObject)
		 */
		@Override
		public void iHaveReceived(RemoteDevice d, RemoteObject o)
		{
			// TODO Auto-generated method stub
			
		}

		/* (non-Javadoc)
		 * @see com.serotonin.bacnet4j.event.DeviceEventListener#covNotificationReceived(com.serotonin.bacnet4j.type.primitive.UnsignedInteger, com.serotonin.bacnet4j.RemoteDevice, com.serotonin.bacnet4j.type.primitive.ObjectIdentifier, com.serotonin.bacnet4j.type.primitive.UnsignedInteger, com.serotonin.bacnet4j.type.constructed.SequenceOf)
		 */
		@Override
		public void covNotificationReceived(
				UnsignedInteger subscriberProcessIdentifier,
				RemoteDevice initiatingDevice,
				ObjectIdentifier monitoredObjectIdentifier,
				UnsignedInteger timeRemaining,
				SequenceOf<PropertyValue> listOfValues)
		{
			// TODO Auto-generated method stub
			
		}

		/* (non-Javadoc)
		 * @see com.serotonin.bacnet4j.event.DeviceEventListener#eventNotificationReceived(com.serotonin.bacnet4j.type.primitive.UnsignedInteger, com.serotonin.bacnet4j.RemoteDevice, com.serotonin.bacnet4j.type.primitive.ObjectIdentifier, com.serotonin.bacnet4j.type.constructed.TimeStamp, com.serotonin.bacnet4j.type.primitive.UnsignedInteger, com.serotonin.bacnet4j.type.primitive.UnsignedInteger, com.serotonin.bacnet4j.type.enumerated.EventType, com.serotonin.bacnet4j.type.primitive.CharacterString, com.serotonin.bacnet4j.type.enumerated.NotifyType, com.serotonin.bacnet4j.type.primitive.Boolean, com.serotonin.bacnet4j.type.enumerated.EventState, com.serotonin.bacnet4j.type.enumerated.EventState, com.serotonin.bacnet4j.type.notificationParameters.NotificationParameters)
		 */
		@Override
		public void eventNotificationReceived(UnsignedInteger processIdentifier,
				RemoteDevice initiatingDevice,
				ObjectIdentifier eventObjectIdentifier, TimeStamp timeStamp,
				UnsignedInteger notificationClass, UnsignedInteger priority,
				EventType eventType, CharacterString messageText,
				NotifyType notifyType, Boolean ackRequired, EventState fromState,
				EventState toState, NotificationParameters eventValues)
		{
			// TODO Auto-generated method stub
			
		}

		/* (non-Javadoc)
		 * @see com.serotonin.bacnet4j.event.DeviceEventListener#textMessageReceived(com.serotonin.bacnet4j.RemoteDevice, com.serotonin.bacnet4j.type.constructed.Choice, com.serotonin.bacnet4j.type.enumerated.MessagePriority, com.serotonin.bacnet4j.type.primitive.CharacterString)
		 */
		@Override
		public void textMessageReceived(RemoteDevice textMessageSourceDevice,
				Choice messageClass, MessagePriority messagePriority,
				CharacterString message)
		{
			// TODO Auto-generated method stub
			
		}

		/* (non-Javadoc)
		 * @see com.serotonin.bacnet4j.event.DeviceEventListener#privateTransferReceived(com.serotonin.bacnet4j.type.primitive.UnsignedInteger, com.serotonin.bacnet4j.type.primitive.UnsignedInteger, com.serotonin.bacnet4j.type.Encodable)
		 */
		@Override
		public void privateTransferReceived(UnsignedInteger vendorId,
				UnsignedInteger serviceNumber, Encodable serviceParameters)
		{
			// TODO Auto-generated method stub
			
		}

		/* (non-Javadoc)
		 * @see com.serotonin.bacnet4j.event.DeviceEventListener#reinitializeDevice(com.serotonin.bacnet4j.service.confirmed.ReinitializeDeviceRequest.ReinitializedStateOfDevice)
		 */
		@Override
		public void reinitializeDevice(
				ReinitializedStateOfDevice reinitializedStateOfDevice)
		{
			// TODO Auto-generated method stub
			
		}

		/* (non-Javadoc)
		 * @see com.serotonin.bacnet4j.event.DeviceEventListener#synchronizeTime(com.serotonin.bacnet4j.type.constructed.DateTime, boolean)
		 */
		@Override
		public void synchronizeTime(DateTime dateTime, boolean utc)
		{
			// TODO Auto-generated method stub
			
		}

		/* (non-Javadoc)
		 * @see com.serotonin.bacnet4j.event.DeviceEventListener#whoIsReceived(com.serotonin.bacnet4j.type.primitive.UnsignedInteger, com.serotonin.bacnet4j.type.primitive.UnsignedInteger)
		 */
		@Override
		public void whoIsReceived(UnsignedInteger deviceInstanceRangeLowLimit,
				UnsignedInteger deviceInstanceRangeHighLimit)
		{
			// TODO Auto-generated method stub
			
		}
	}
}
