/*
 * File: ForeignObject.java
 * Project: org.unirostock.bagateway.bacnet
 * @author Robert Balla
 */
package org.unirostock.bagateway.bacnet;

import java.util.Hashtable;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;
import java.util.Map.Entry;

import org.unirostock.bagateway.bacnet.tables.ParameterTable;
import org.unirostock.bagateway.bacnet.tables.UnitTable;
import org.unirostock.bagateway.entitiy.GatewayInternalEntity;
import org.unirostock.bagateway.entitiy.ParameterEntity;
import org.unirostock.bagateway.entitiy.SubEntity;
import org.unirostock.bagateway.enums.GWparameter;
import org.unirostock.bagateway.enums.GWunit;
import org.unirostock.bagateway.interfaces.GatewayInterface;

import com.serotonin.bacnet4j.LocalDevice;
import com.serotonin.bacnet4j.RemoteDevice;
import com.serotonin.bacnet4j.enums.MaxApduLength;
import com.serotonin.bacnet4j.event.ExceptionDispatch;
import com.serotonin.bacnet4j.exception.BACnetException;
import com.serotonin.bacnet4j.exception.BACnetServiceException;
import com.serotonin.bacnet4j.exception.PropertyValueException;
import com.serotonin.bacnet4j.obj.BACnetObject;
import com.serotonin.bacnet4j.obj.ObjectCovSubscription;
import com.serotonin.bacnet4j.obj.ObjectProperties;
import com.serotonin.bacnet4j.obj.PropertyTypeDefinition;
import com.serotonin.bacnet4j.service.confirmed.ConfirmedCovNotificationRequest;
import com.serotonin.bacnet4j.service.unconfirmed.UnconfirmedCovNotificationRequest;
import com.serotonin.bacnet4j.type.Encodable;
import com.serotonin.bacnet4j.type.constructed.PriorityArray;
import com.serotonin.bacnet4j.type.constructed.PropertyValue;
import com.serotonin.bacnet4j.type.constructed.SequenceOf;
import com.serotonin.bacnet4j.type.constructed.StatusFlags;
import com.serotonin.bacnet4j.type.enumerated.BinaryPV;
import com.serotonin.bacnet4j.type.enumerated.EngineeringUnits;
import com.serotonin.bacnet4j.type.enumerated.ErrorClass;
import com.serotonin.bacnet4j.type.enumerated.ErrorCode;
import com.serotonin.bacnet4j.type.enumerated.PropertyIdentifier;
import com.serotonin.bacnet4j.type.enumerated.Segmentation;
import com.serotonin.bacnet4j.type.primitive.CharacterString;
import com.serotonin.bacnet4j.type.primitive.Date;
import com.serotonin.bacnet4j.type.primitive.ObjectIdentifier;
import com.serotonin.bacnet4j.type.primitive.Real;
import com.serotonin.bacnet4j.type.primitive.SignedInteger;
import com.serotonin.bacnet4j.type.primitive.Time;
import com.serotonin.bacnet4j.type.primitive.UnsignedInteger;

// TODO: Auto-generated Javadoc
/**
 * The Class ForeignObject.
 */
public class ForeignObject extends BACnetObject
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private GatewayInterface gateway;
	
	private LinkedList<Object> path;
	
	private LinkedList<ForeignProperty> foreignProperties;
	
	private boolean finalized = false;
	
	/**
	 * Instantiates a new foreign object.
	 *
	 * @param localDevice the local device
	 * @param id the id
	 * @param ent the ent
	 * @throws BACnetServiceException 
	 */
	public ForeignObject(LocalDevice localDevice, ObjectIdentifier id, SubEntity ent, String name, GatewayInterface gateway) throws BACnetServiceException
	{
		super(localDevice, id);
		
		this.path = ent.getPath();
		this.gateway = gateway;
		this.foreignProperties = new LinkedList<ForeignProperty>();
		
		this.setProperty(PropertyIdentifier.objectName, new CharacterString(name));
		this.setProperty(PropertyIdentifier.statusFlags, new StatusFlags(false, false, false, false));
		this.setProperty(PropertyIdentifier.description, new CharacterString("This is the BACnet-representation of the foreign device " + ent.getOriginIdentifier().toString()));
		
	}
	
	public void addProperty(ForeignProperty prop)
	{
		foreignProperties.add(prop);
	}

	
	@Override
	public Encodable getProperty(PropertyIdentifier pid) throws BACnetServiceException
	{
		if (pid.intValue() == PropertyIdentifier.objectIdentifier.intValue())
         return this.getId();
		if (pid.intValue() == PropertyIdentifier.objectType.intValue())
         return this.getId().getObjectType();
		
		// Check that the requested property is valid for the object. This will throw an exception if the
      // property doesn't belong.
      ObjectProperties.getPropertyTypeDefinitionRequired(this.getId().getObjectType(), pid);
      
      // Do some property-specific checking here.
      if (pid.intValue() == PropertyIdentifier.localTime.intValue())
          return new Time();
      if (pid.intValue() == PropertyIdentifier.localDate.intValue())
          return new Date();
      
      
      if (this.finalized && (foreignProperties.contains(new ForeignProperty(pid))))
      {
      	ForeignProperty prop = foreignProperties.get(foreignProperties.indexOf(new ForeignProperty(pid)));
      	PropertyTypeDefinition propDef = ObjectProperties.getPropertyTypeDefinition(this.getId().getObjectType(), prop.getPropertyIdentifier());
      	
      	if (!prop.isReadable())
      		throw new BACnetServiceException(ErrorClass.property, ErrorCode.valueNotInitialized);

      	Hashtable<Object, String> request = new Hashtable<Object, String>();
      	request.put(prop.getPath().getLast(), "");
      	
      	request = gateway.readValue(this.path, request);
      	String value = request.get(prop.getPath().getLast());
      	
      	Encodable result = null;
   		if (pid.equals(PropertyIdentifier.units))
   		{
   			result = UnitTable.gw2bac.get(GWunit.valueOf(value));
   		}else
   		{
   			result = BACnetHelper.convertValueToEncodable(value, propDef.getClazz());
   		}
   		  	
      	return result;
      }
		
		return this.properties.get(pid);
	}
	
	@Override
   public void setProperty(PropertyIdentifier pid, Encodable val) throws BACnetServiceException {
      ObjectProperties.validateValue(this.getId().getObjectType(), pid, val);
      
      if(this.finalized && foreignProperties.contains(new ForeignProperty(pid)))
      {
      	ForeignProperty prop = foreignProperties.get(foreignProperties.indexOf(new ForeignProperty(pid)));
      	Hashtable<Object, String> request = new Hashtable<Object, String>();
      	
      	String value = null;
      	if (pid.equals(PropertyIdentifier.units))
			{
				value = UnitTable.bac2gw.get((EngineeringUnits) val).toString();
			}else
			{
				value = val.toString();
			}
      	
      	request.put(prop.getPath().getLast(), value);
      	gateway.writeValue(this.path, request);
      }
      else
      {
      	this.setPropertyImpl(pid, val);
      }

      // If the relinquish default was set, make sure the present value gets updated as necessary.
//      if (pid.equals(PropertyIdentifier.relinquishDefault))
//          this.setCommandableImpl((PriorityArray) getProperty(PropertyIdentifier.priorityArray));
	}
	
	public void subscribe(UnsignedInteger duration)
	{
		gateway.subscribe(this.path, duration.longValue()*1000);
	}
	
	public void sendNotification(Hashtable<Object, String> value, LocalDevice gateDevice)
	{         
	   synchronized (this.covSubscriptions)
	   {
	   	long now = System.currentTimeMillis();
         ObjectCovSubscription sub;
         for (int i = covSubscriptions.size() - 1; i >= 0; i--) {
             sub = covSubscriptions.get(i);
             if (sub.hasExpired(now))
             {
                 covSubscriptions.remove(i);
             } else
             {
            	 try {
                   UnsignedInteger timeLeft = new UnsignedInteger(sub.getTimeRemaining(now));
                   SequenceOf<PropertyValue> values = new SequenceOf<PropertyValue>();
                   
                   Iterator<Entry<Object, String>> entries = value.entrySet().iterator();
             		 while (entries.hasNext())
             		 {
             			 Map.Entry<Object, String> prop = (Entry<Object, String>) entries.next();
             			 PropertyIdentifier propId = ParameterTable.gw2bac.get(prop.getKey());             			 
             			 if (propId == null)
             				 propId = PropertyIdentifier.presentValue;
             			 
             			 if (!this.foreignProperties.contains(new ForeignProperty(propId)))
             				 continue;
             			
             			Class<? extends Encodable> typeId = ObjectProperties.getPropertyTypeDefinition(this.getId().getObjectType(), propId).getClazz();
             			 
             			Encodable val = null;
            			if (propId.equals(PropertyIdentifier.units))
            			{
            				val = UnitTable.gw2bac.get(GWunit.valueOf(prop.getValue()));
            			}else
            			{
            				val = BACnetHelper.convertValueToEncodable(prop.getValue(), typeId);
            			}
             			 
             			values.add(new PropertyValue(propId, BACnetHelper.convertValueToEncodable(prop.getValue(), typeId)));
             		 }
                   
                   if (sub.isIssueConfirmedNotifications()) {
                       // Confirmed
                       ConfirmedCovNotificationRequest req = new ConfirmedCovNotificationRequest(
                               sub.getSubscriberProcessIdentifier(), localDevice.getConfiguration().getId(), id, timeLeft,
                               values);
                       RemoteDevice d = gateDevice.getRemoteDevice(sub.getAddress());
                       if (d == null)
                           localDevice.sendConfirmed(sub.getAddress(), sub.getLinkService(), MaxApduLength.UP_TO_50,
                                   Segmentation.noSegmentation, req);
                       else
                           localDevice.sendConfirmed(d, req);
                   }
                   else {
                       // Unconfirmed
                       UnconfirmedCovNotificationRequest req = new UnconfirmedCovNotificationRequest(
                               sub.getSubscriberProcessIdentifier(), localDevice.getConfiguration().getId(), id, timeLeft,
                               values);
                       localDevice.sendUnconfirmed(sub.getAddress(), sub.getLinkService(), req);
                   }
               }
               catch (BACnetException e) {
                   ExceptionDispatch.fireReceivedException(e);
               }
             }
         }
	   }
         	
	}
	
	public boolean isWriteable(PropertyIdentifier propId)
	{
		ForeignProperty prop = foreignProperties.get(foreignProperties.indexOf(new ForeignProperty(propId)));
		
		return prop.isWriteable();
	}
	
	public boolean isSubscribable()
	{
		ForeignProperty prop = foreignProperties.get(foreignProperties.indexOf(new ForeignProperty(PropertyIdentifier.presentValue)));
		
		return prop.isSubscribable();
	}
	
	public void finalize()
	{
		finalized = true;
	}
}
