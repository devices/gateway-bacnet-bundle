/*
 * File: GateDevice.java
 * Project: org.unirostock.bagateway.bacnet
 * @author Robert Balla
 */
package org.unirostock.bagateway.bacnet;

import com.serotonin.bacnet4j.LocalDevice;
import com.serotonin.bacnet4j.RemoteDevice;
import com.serotonin.bacnet4j.transport.Transport;

// TODO: Auto-generated Javadoc
/**
 * The Class GateDevice.
 */
public class GateDevice extends LocalDevice
{

	/**
	 * Instantiates a new gate device.
	 *
	 * @param deviceId the device id
	 * @param transport the transport
	 */
	public GateDevice(int deviceId, Transport transport)
	{
		super(deviceId, transport);
	}
	
}
