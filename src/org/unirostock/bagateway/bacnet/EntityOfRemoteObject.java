/*
 * File: EntityOfObject.java
 * Project: org.unirostock.bagateway.bacnet
 * @author Robert Balla
 */
package org.unirostock.bagateway.bacnet;

import java.util.LinkedList;
import java.util.List;
import java.util.logging.Logger;

import org.unirostock.bagateway.bacnet.tables.GroupTable;
//import org.unirostock.bagateway.bacnet.tables.PropertyTable;
//import org.unirostock.bagateway.bacnet.tables.PropertyTable.BACnetPropertyInfo;
import org.unirostock.bagateway.bacnet.tables.DatatypeTable;
import org.unirostock.bagateway.bacnet.tables.ParameterTable;
import org.unirostock.bagateway.entitiy.SubEntity;
import org.unirostock.bagateway.enums.GWdatatype;
import org.unirostock.bagateway.enums.GWdirection;
import org.unirostock.bagateway.enums.GWgroup;

import com.serotonin.bacnet4j.LocalDevice;
import com.serotonin.bacnet4j.RemoteDevice;
import com.serotonin.bacnet4j.RemoteObject;
import com.serotonin.bacnet4j.exception.BACnetException;
import com.serotonin.bacnet4j.obj.ObjectProperties;
import com.serotonin.bacnet4j.obj.PropertyTypeDefinition;
import com.serotonin.bacnet4j.type.Encodable;
import com.serotonin.bacnet4j.type.enumerated.ObjectType;
import com.serotonin.bacnet4j.type.enumerated.PropertyIdentifier;
import com.serotonin.bacnet4j.util.RequestUtils;

// TODO: Auto-generated Javadoc
/**
 * The Class EntityOfObject.
 */
public class EntityOfRemoteObject extends SubEntity
{
	
	/** The Constant log. */
	private static final Logger log = Logger.getLogger(BACnetGateImpl.class.getName());
	
	/**
	 * Instantiates a new entity of object.
	 *
	 * @param gateDev the gate device
	 * @param remDev the remote device
	 * @param obj the object
	 * @param path the path
	 */
	public EntityOfRemoteObject(LocalDevice gateDev, RemoteDevice remDev, RemoteObject obj, LinkedList<Object> path)
	{	
		super(obj.getObjectName(), obj.getObjectIdentifier(), 1, path);
		
		List<PropertyTypeDefinition> propertyArray = ObjectProperties.getPropertyTypeDefinitions(obj.getObjectIdentifier().getObjectType());
		//BACnetPropertyInfo[] propertyArray = getProperties(obj.getObjectIdentifier().getObjectType());
		Encodable propVal;
		GWgroup item;
		
		if (propertyArray != null)
		{
			try
			{
				boolean covSupported = remDev.getServicesSupported().isConfirmedCovNotification();
				boolean writeSupported = remDev.getServicesSupported().isWriteProperty();
				
				this.setGroup(solveGroup(obj.getObjectIdentifier().getObjectType(), gateDev, remDev, obj));
				this.setCount(obj.getObjectIdentifier().getInstanceNumber());
				this.setDirection(solveDirection(obj.getObjectIdentifier().getObjectType()));
				
				for (PropertyTypeDefinition propInfo : propertyArray)
				{			
					PropertyIdentifier propId = propInfo.getPropertyIdentifier();
					Class<? extends Encodable> typeId = propInfo.getClazz();
					
					propVal = RequestUtils.sendReadPropertyAllowNull(gateDev, remDev, obj.getObjectIdentifier(), propId, null, null);
					
					/*
					 * check if the value was read
					 * if not, the property doesn't exist probably 
					 */
					if (propVal != null)
					{
						this.addParameter(propId.toString(), propId);
						this.getParameter(propId).setParameterName(ParameterTable.bac2gw.get(propId));
						this.getParameter(propId).setReadable(true);
						this.getParameter(propId).setLastValue(propVal.toString());
						
						if (covSupported)
							this.getParameter(propId).setSubscribable(this.isSubscribable(obj.getObjectIdentifier().getObjectType(), propId));
												
						this.getParameter(propId).setDatatype(DatatypeTable.bac2gw.get(typeId));
						this.getParameter(propId).setParameterName(ParameterTable.bac2gw.get(propId));
						
						if (writeSupported)
						{
							try{
								RequestUtils.writeProperty(gateDev, remDev, obj.getObjectIdentifier(), propId, propVal);
								this.getParameter(propId).setWriteable(true);	
							}catch (BACnetException e1)
							{
								//e1.printStackTrace();
							}
						}
					}
				}
			} catch (BACnetException e1)
			{
				log.warning("Gate wasn't able to determine all attributes. Entity may faulty!");
				e1.printStackTrace();
			}
		}
	}
	
//	/**
//	 * Gets the properties.
//	 *
//	 * @param objType the obj type
//	 * @return the properties
//	 */
//	private BACnetPropertyInfo[] getProperties(ObjectType objType)
//	{
//		if (objType.equals(ObjectType.analogInput))
//		{
//			return PropertyTable.AnalogInputProperties;
//		}
//		else if (objType.equals(ObjectType.analogOutput))
//		{
//			return PropertyTable.AnalogOutputProperties;
//		}
//		else if (objType.equals(ObjectType.analogValue))
//		{
//			return PropertyTable.AnalogValueProperties;
//		}
//		else if (objType.equals(ObjectType.binaryInput))
//		{
//			return PropertyTable.BinaryInputProperties;
//		}
//		else if (objType.equals(ObjectType.binaryOutput))
//		{
//			return PropertyTable.BinaryOutputProperties;
//		}
//		else if (objType.equals(ObjectType.binaryValue))
//		{
//			return PropertyTable.BinaryValueProperties;
//		}
//		else if (objType.equals(ObjectType.multiStateInput))
//		{
//			return PropertyTable.MultiStateInputProperties;
//		}
//		else if (objType.equals(ObjectType.multiStateOutput))
//		{
//			return PropertyTable.MultiStateOutputProperties;
//		}
//		else if (objType.equals(ObjectType.multiStateValue))
//		{
//			return PropertyTable.MultiStateValueProperties;
//		}
//		
//		return null;
//	}
	
	/**
	 * Checks if is orderable.
	 *
	 * @param objType the obj type
	 * @param propId the prop id
	 * @return the boolean
	 */
	private Boolean isSubscribable(ObjectType objType, PropertyIdentifier propId)
	{
		if (objType.equals(ObjectType.analogInput) && (propId.equals(PropertyIdentifier.presentValue) || propId.equals(PropertyIdentifier.statusFlags)))
		{
			return true;
		}
		else if (objType.equals(ObjectType.analogOutput) && (propId.equals(PropertyIdentifier.presentValue) || propId.equals(PropertyIdentifier.statusFlags)))
		{
			return true;
		}
		else if (objType.equals(ObjectType.analogValue) && (propId.equals(PropertyIdentifier.presentValue) || propId.equals(PropertyIdentifier.statusFlags)))
		{
			return true;
		}
		else if (objType.equals(ObjectType.binaryInput) && (propId.equals(PropertyIdentifier.presentValue) || propId.equals(PropertyIdentifier.statusFlags)))
		{
			return true;
		}
		else if (objType.equals(ObjectType.binaryOutput) && (propId.equals(PropertyIdentifier.presentValue) || propId.equals(PropertyIdentifier.statusFlags)))
		{
			return true;
		}
		else if (objType.equals(ObjectType.binaryValue) && (propId.equals(PropertyIdentifier.presentValue) || propId.equals(PropertyIdentifier.statusFlags)))
		{
			return true;
		}
		else if (objType.equals(ObjectType.multiStateInput) && (propId.equals(PropertyIdentifier.presentValue) || propId.equals(PropertyIdentifier.statusFlags)))
		{
			return true;
		}
		else if (objType.equals(ObjectType.multiStateOutput) && (propId.equals(PropertyIdentifier.presentValue) || propId.equals(PropertyIdentifier.statusFlags)))
		{
			return true;
		}
		else if (objType.equals(ObjectType.multiStateValue) && (propId.equals(PropertyIdentifier.presentValue) || propId.equals(PropertyIdentifier.statusFlags)))
		{
			return true;
		}
		return false;
	}

	private GWdirection solveDirection (ObjectType objType)
	{
		if (objType.equals(ObjectType.analogInput) || objType.equals(ObjectType.binaryInput) || objType.equals(ObjectType.multiStateInput))
		{
			return GWdirection.INPUT;
		}
		else if (objType.equals(ObjectType.analogOutput) || objType.equals(ObjectType.binaryOutput) || objType.equals(ObjectType.multiStateOutput))
		{
			return GWdirection.OUTPUT;
		}
		else if (objType.equals(ObjectType.analogValue) || objType.equals(ObjectType.binaryValue) || objType.equals(ObjectType.multiStateValue))
		{
			return GWdirection.VALUE;
		}
		
		return null;
	}
	
	/**
	 * Gets the item name.
	 *
	 * @param objType the obj type
	 * @param gateDev the gate dev
	 * @param remDev the rem dev
	 * @param obj the obj
	 * @return the item name
	 */
	private GWgroup solveGroup(ObjectType objType, LocalDevice gateDev, RemoteDevice remDev, RemoteObject obj)
	{
		if (objType.equals(ObjectType.analogInput) || objType.equals(ObjectType.analogOutput) || objType.equals(ObjectType.analogValue))
		{
			Encodable unit = null;
			try
			{
				unit = RequestUtils.sendReadPropertyAllowNull(gateDev, remDev, obj.getObjectIdentifier(), PropertyIdentifier.units, null, null);
			} catch (BACnetException e)
			{
				log.warning("Couldn't read unit of analog-object!");
				e.printStackTrace();
			}
			
			return GroupTable.bac2gw.get(unit);
		}
		else if (objType.equals(ObjectType.binaryInput) || objType.equals(ObjectType.binaryOutput) || objType.equals(ObjectType.binaryValue))
		{
			return GWgroup.BINARY;
		}
		else if (objType.equals(ObjectType.multiStateInput) || objType.equals(ObjectType.multiStateOutput) || objType.equals(ObjectType.multiStateValue))
		{
			return GWgroup.MULTISTATE;
		}

		return null;
	}
}
