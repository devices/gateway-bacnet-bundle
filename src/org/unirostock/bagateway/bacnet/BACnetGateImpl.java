/*
 * File: BACnetGateImpl.java
 * Project: org.uniRostock.bacnetDPWS.bacnetGate
 * @author Robert Balla
 */
package org.unirostock.bagateway.bacnet;

import java.io.IOException;
import java.net.InterfaceAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.logging.FileHandler;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

import com.serotonin.bacnet4j.RemoteDevice;
import com.serotonin.bacnet4j.RemoteObject;
import com.serotonin.bacnet4j.event.DeviceEventListener;
import com.serotonin.bacnet4j.exception.BACnetException;
import com.serotonin.bacnet4j.exception.PropertyValueException;
import com.serotonin.bacnet4j.npdu.ip.IpNetwork;
import com.serotonin.bacnet4j.obj.BACnetObject;
import com.serotonin.bacnet4j.obj.ObjectProperties;
import com.serotonin.bacnet4j.obj.PropertyTypeDefinition;
import com.serotonin.bacnet4j.service.confirmed.ReinitializeDeviceRequest.ReinitializedStateOfDevice;
import com.serotonin.bacnet4j.service.confirmed.SubscribeCOVRequest;
import com.serotonin.bacnet4j.service.unconfirmed.WhoHasRequest;
import com.serotonin.bacnet4j.service.unconfirmed.WhoIsRequest;
import com.serotonin.bacnet4j.transport.Transport;
import com.serotonin.bacnet4j.type.Encodable;
import com.serotonin.bacnet4j.type.constructed.Choice;
import com.serotonin.bacnet4j.type.constructed.DateTime;
import com.serotonin.bacnet4j.type.constructed.PropertyValue;
import com.serotonin.bacnet4j.type.constructed.SequenceOf;
import com.serotonin.bacnet4j.type.constructed.TimeStamp;
import com.serotonin.bacnet4j.type.enumerated.EngineeringUnits;
import com.serotonin.bacnet4j.type.enumerated.EventState;
import com.serotonin.bacnet4j.type.enumerated.EventType;
import com.serotonin.bacnet4j.type.enumerated.MessagePriority;
import com.serotonin.bacnet4j.type.enumerated.NotifyType;
import com.serotonin.bacnet4j.type.enumerated.PropertyIdentifier;
import com.serotonin.bacnet4j.type.notificationParameters.NotificationParameters;
import com.serotonin.bacnet4j.type.primitive.Boolean;
import com.serotonin.bacnet4j.type.primitive.CharacterString;
import com.serotonin.bacnet4j.type.primitive.ObjectIdentifier;
import com.serotonin.bacnet4j.type.primitive.UnsignedInteger;
import com.serotonin.bacnet4j.util.DiscoveryUtils;
import com.serotonin.bacnet4j.util.PropertyReferences;
import com.serotonin.bacnet4j.util.PropertyValues;
import com.serotonin.bacnet4j.util.RequestUtils;

import org.unirostock.bagateway.bacnet.tables.UnitTable;
import org.unirostock.bagateway.entitiy.RootEntity;
import org.unirostock.bagateway.enums.GWunit;
import org.unirostock.bagateway.interfaces.GateInterface;
import org.unirostock.bagateway.interfaces.GatewayInterface;

/**
 * The Class BACnetGateImpl.
 * 
 * this class enclose the full functionality of the BACnet-gate.
 */
public class BACnetGateImpl implements GateInterface
{
	
	/** The Constant log. */
	private static final Logger log = Logger.getLogger(BACnetGateImpl.class.getName());
	
	/** The Constant PROTOCOL. */
	static final String PROTOCOL = "BACnet";
	
	/** The Constant MAX_DEVICEID.
	 * 
	 *  the greatest possible device-id/object-id is 0x3FFFF (262143)
	 *  */
	static final Integer MAX_DEVICEID = 0x3FFFFF;
	
	/** The gateway. 
	 * 
	 * represents the gateway itself
	 * */
	private GatewayInterface gateway;
	
	/** The gate device. 
	 * 
	 * the BACnet-device which represents the gateway in the BACnet-network
	 * */
   final GateDevice gateDevice;
	
	/** The device id. 
	 * 
	 * contains the last assigned id for devices 
	 * */
	private Integer deviceID;
	
	/** The used device IDs. 
	 * 
	 * contains all IDs of known devices
	 * */
   final LinkedList<Integer> usedDeviceIDs;
   
   /** The broadcast address. */
   final String broadcastAddress; 
   
   /** The free ports. 
    * 
    * contains the port-numbers from 0xBAC1 to 0xBAC1+1000
    * */
   final LinkedList<Integer> freePorts;
   
   final Hashtable<UnsignedInteger, COVSubscription> covSubscriptions;
   
   /** The foreign devices. 
    * 
    * contains all devices which are not BACnet originally
    * */
   final Hashtable<Integer, ForeignDevice> foreignDevices;
	
	/**
	 * Instantiates a new BACnet-Gate.
	 */
	public BACnetGateImpl()
	{
		this.makeLogFile();
		
		/*
		 * initialize the variables 
		 */
		gateway = null;
		deviceID = MAX_DEVICEID;
		broadcastAddress = getBroadcastAddress();
		if (broadcastAddress == null)
			log.info("Can't get Broadcastaddress");
		
		foreignDevices = new Hashtable<Integer, ForeignDevice>();
		usedDeviceIDs = new LinkedList<Integer>();
		freePorts = new LinkedList<Integer>();
		covSubscriptions = new Hashtable<UnsignedInteger, COVSubscription>();
		
		/*
		 * fill freePorts with the port-numbers 0xBAC1 to 0xBAC1+1000
		 */
		Integer port = IpNetwork.DEFAULT_PORT + 1;
		while (port <= IpNetwork.DEFAULT_PORT + 1000)
		{
			freePorts.add(port);
			port++;
		}
		
		/*
		 * instantiate the gateDevice
		 */
		IpNetwork network = new IpNetwork(broadcastAddress);
		Transport transport = new Transport(network);						
		gateDevice = new GateDevice(deviceID, transport);
		
		usedDeviceIDs.addLast(deviceID);
	}

	/**
	 * Sets the gateway.
	 *
	 * @param gw the new gateway
	 */
	public void setGateway(GatewayInterface gw)
	{
			gateway = gw;
	}
	
	/**
	 * Gets the gateway.
	 *
	 * @return the gateway
	 */
	public GatewayInterface getGateway()
	{
			return gateway;
	}
	
	/* (non-Javadoc)
	 * @see org.unirostock.bacnetdpws.GateInterface#openGate()
	 */
	@Override
	public void openGate()
	{	
		/*
		 * initialize the gateDevice
		 * add the listener (on port 0xBAC0)
		 * send an iAm-request
		 */
		try {
			gateDevice.initialize();
			gateDevice.getEventHandler().addListener(new BACnetListener());
			gateDevice.sendGlobalBroadcast(gateDevice.getIAm());
			
			log.info("BACnet-Gate is open");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/* (non-Javadoc)
	 * @see org.unirostock.bacnetdpws.GateInterface#closeGate()
	 */
	@Override
	public void closeGate()
	{
		/*
		 * terminate the gateDevice
		 */
		gateDevice.terminate();
		
		log.info("BACnet-Gate is closed");
	}
	
	/* (non-Javadoc)
	 * @see org.unirostock.bagateway.interfaces.GateInterface#newDevice(org.unirostock.bagateway.entitiy.RootEntity)
	 */
	@Override
	public RootEntity newDevice(RootEntity ent)
	{
		/*
		 * check if there is a unused deviceId and a free port
		 */
		if ((usedDeviceIDs.size() == MAX_DEVICEID) || freePorts.isEmpty())
		{
			log.info("No more Devices possible!");
			return null;
		}
		
		/*
		 * look for an unused deviceID
		 */
		do
		{
			if (deviceID == 0)
				deviceID = MAX_DEVICEID;
			
			deviceID--;
		} while (usedDeviceIDs.contains(deviceID));
		
		/*
		 * get a free port-number
		 */
		Integer port = freePorts.getFirst();
		
		/*
		 * instantiate a new device which is not BACnet originally
		 */
		IpNetwork ipnet = new IpNetwork (broadcastAddress, port);
		Transport transp = new Transport(ipnet);
		ForeignDevice device = new ForeignDevice(deviceID, transp, ent, this.broadcastAddress, this.gateway);

		/*
		 * put the new device to the list
		 * add the used device-id to the used IDs
		 * remove the used port from the list
		 */
		usedDeviceIDs.add(deviceID);
		freePorts.remove(port);
		foreignDevices.put(deviceID, device);
		
		/*
		 * add the BACnet-identifier to the entity
		 */
		ent.addIdentifier(PROTOCOL, device.getConfiguration().getId());
		
		return ent;
	}
	
	/* (non-Javadoc)
	 * @see org.unirostock.bacnetdpws.GateInterface#announceDev(java.lang.Object)
	 */
	@Override
	public void announceDevice(RootEntity ent)
	{
		ForeignDevice dev = foreignDevices.get(((ObjectIdentifier) ent.getIdentifier(PROTOCOL)).getInstanceNumber());
		
		dev.sendIAm();
	}

	/* (non-Javadoc)
	 * @see org.unirostock.bacnetdpws.GateInterface#discoverDev(java.lang.String)
	 */
	@Override
	public void discoverDevice(String name)
	{
		/*
		 * if there is no name to search for, send a general whoIs-request
		 * else ask for the device who has an object with this name
		 */
		if (name == null)
		{
			try {
				gateDevice.sendGlobalBroadcast(new WhoIsRequest());
			} catch (BACnetException e)
			{
				log.warning("Gate wasn't able to send WhoIs-Request!");
				e.printStackTrace();
			}
		} else
		{
			try
			{
				gateDevice.sendGlobalBroadcast(new WhoHasRequest(null, new CharacterString(name)));
			} catch (BACnetException e) {
				log.warning("Gate wasn't able to send WhoHas-Request for searching a device by name!");
				e.printStackTrace();
			}
		}
	}
	
	/* (non-Javadoc)
	 * @see org.unirostock.bagateway.interfaces.GateInterface#readValue(org.unirostock.bagateway.entitiy.RootEntity, org.unirostock.bagateway.entitiy.GatewayInternalEntity)
	 */
	@Override
	public Hashtable<Object, String> readValue(RootEntity rootEnt, LinkedList<Object> path, Hashtable<Object, String> request)
	{
		String result = null;
		
		/*
		 * get the device, object and property which is asked for
		 */
		ObjectIdentifier deviceId = (ObjectIdentifier) rootEnt.getOriginIdentifier();
		ObjectIdentifier objectId = (ObjectIdentifier) path.getLast();
		
		/*
		 * send a readProperty-request to the device
		 */
		Iterator<Entry<Object, String>> entries = request.entrySet().iterator();
		while (entries.hasNext())
		{
			Map.Entry<Object, String> prop = (Entry<Object, String>) entries.next();
			PropertyIdentifier propId = (PropertyIdentifier) prop.getKey();
			
			try
			{
				RemoteDevice d = gateDevice.getRemoteDevice(deviceId.getInstanceNumber());
				
				PropertyReferences propRef = new PropertyReferences();
				
				propRef.add(objectId, propId);
				PropertyValues val = RequestUtils.readProperties(gateDevice, d, propRef, null);
				
				if (propId.equals(PropertyIdentifier.units))
				{
					try
					{
						GWunit unit = UnitTable.bac2gw.get((EngineeringUnits) val.get(objectId, propId));
						if (unit != null)
						{
							result = unit.toString();
						}else
						{
							result = val.getString(objectId, propId);
						}
					} catch (PropertyValueException e)
					{
						
						e.printStackTrace();
					}
				}else
				{
					result = val.getString(objectId, propId);
				}
				
				gateway.setLastValueOfParameter(path, prop.getKey(), result);
				request.put(propId, result);
			} catch (BACnetException e)
			{
				log.warning("Gate wasn't able to read property " + propId.toString() + "!");
				e.printStackTrace();
			}
		}
		
		return request;
	}
	
	/* (non-Javadoc)
	 * @see org.unirostock.bagateway.interfaces.GateInterface#writeValue(org.unirostock.bagateway.entitiy.RootEntity, org.unirostock.bagateway.entitiy.GatewayInternalEntity, java.lang.String)
	 */
	@Override
	public void writeValue(RootEntity rootEnt, LinkedList<Object> path, Hashtable<Object, String> request)
	{
		ObjectIdentifier deviceId = (ObjectIdentifier) rootEnt.getOriginIdentifier();
		ObjectIdentifier objectId = (ObjectIdentifier) path.get(1);
		
		/*
		 * send a writeProperty-request to the device
		 */
		Class<? extends Encodable> typeId = null;
		List<PropertyTypeDefinition> propertyArray = ObjectProperties.getPropertyTypeDefinitions(objectId.getObjectType());
		
		Iterator<Entry<Object, String>> entries = request.entrySet().iterator();
		while (entries.hasNext())
		{
			Map.Entry<Object, String> prop = (Entry<Object, String>) entries.next();
			PropertyIdentifier propId = (PropertyIdentifier) prop.getKey();
			
			for (PropertyTypeDefinition propInfo : propertyArray)
			{
				if (propInfo.getPropertyIdentifier().equals(propId))
				{
					typeId = propInfo.getClazz();
					break;
				}
			}
			
			Encodable value = null;
			if (propId.equals(PropertyIdentifier.units))
			{
				value = UnitTable.gw2bac.get(GWunit.valueOf(prop.getValue()));
			}else
			{
				value = BACnetHelper.convertValueToEncodable(prop.getValue(), typeId);
			}
			
			try
			{
				RemoteDevice d = gateDevice.getRemoteDevice(deviceId.getInstanceNumber());
				RequestUtils.writeProperty(gateDevice, d, objectId, propId, value);
				gateway.setLastValueOfParameter(path, prop.getKey(), prop.getValue());
			} catch (BACnetException e)
			{
				log.warning("Gate wasn't able to write property!");
				e.printStackTrace();
			}
		}
	}
	
	@Override
	public void subscribe(RootEntity rootEnt, LinkedList<Object> path, long duration)
	{
		ObjectIdentifier deviceId = (ObjectIdentifier) rootEnt.getOriginIdentifier();
		ObjectIdentifier objectId = (ObjectIdentifier) path.get(1);
		
		COVSubscription subscription = new COVSubscription(path, System.currentTimeMillis(), duration);
		
		RemoteDevice remDev = null;
		try
		{
			remDev = gateDevice.getRemoteDevice(deviceId.getInstanceNumber());
		} catch (BACnetException e)
		{
			log.info("Couldn't find RemoteDevice!");
			e.printStackTrace();
			return;
		}
		
		if (!remDev.getServicesSupported().isConfirmedCovNotification())
		{
			log.info("Device doesn't support COV-Notification!");
			return;
		}
		
		UnsignedInteger covId = new UnsignedInteger(0);
		while (covSubscriptions.containsKey(covId))
		{
			long time = System.currentTimeMillis() - covSubscriptions.get(covId).time;
			if (time > duration)
				covSubscriptions.remove(covId);
			
			covId = new UnsignedInteger(covId.intValue() + 1);
		}
			
		long durationInSec = duration/1000;
		SubscribeCOVRequest req = new SubscribeCOVRequest(covId, objectId, new Boolean(true), new UnsignedInteger(durationInSec));
      try
		{
			gateDevice.send(remDev, req);
			covSubscriptions.put(covId, subscription);
		} catch (BACnetException e)
		{
			e.printStackTrace();
		}
	}

	@Override
	public void notification(RootEntity rootEnt, LinkedList<Object> path,
			Hashtable<Object, String> value)
	{
		ObjectIdentifier deviceId = (ObjectIdentifier) rootEnt.getIdentifier(PROTOCOL);
		ForeignDevice dev = foreignDevices.get(deviceId.getInstanceNumber());
		
		String name = rootEnt.getName();
		Iterator<Object> pathIt = path.iterator();
		pathIt.next();
		while (pathIt.hasNext())
		{
			name = name + "/"	+ rootEnt.getSubEntity(pathIt.next()).getName();
		}
		
		ForeignObject obj = (ForeignObject) dev.getObject(name);
		
		System.out.println(value);
		obj.sendNotification(value, gateDevice);
	}
	
	/**
	 * The listener interface for receiving BACnet events.
	 * The class that is interested in processing a BACnet
	 * event implements this interface, and the object created
	 * with that class is registered with a component using the
	 * component's <code>addBACnetListener<code> method. When
	 * the BACnet event occurs, that object's appropriate
	 * method is invoked.
	 *
	 * @see BACnetEvent
	 */
	class BACnetListener implements DeviceEventListener
	{
     
     /* (non-Javadoc)
      * @see com.serotonin.bacnet4j.event.DeviceEventAdapter#iAmReceived(com.serotonin.bacnet4j.RemoteDevice)
      */
     @Override
     public void iAmReceived(final RemoteDevice remD)
     {
   	  log.info("IAm received from" + remD.toString());
   	  
   	  /*
   	   * check if the iAm-request is from my foreignDevices
   	   */
   	  if (foreignDevices.containsKey(remD.getInstanceNumber()) || (gateDevice.getConfiguration().getInstanceId() == (remD.getInstanceNumber())))
   	  {
   		  log.info("IAm rejected (UUID of Gate-Device or Foreign-Device)");
   		  return;
   	  }
   	  
   	  /*
   	   * add the deviceID to the used ones
   	   */
   	  final ObjectIdentifier deviceId = remD.getObjectIdentifier();
   	  if (!usedDeviceIDs.contains(deviceId.getInstanceNumber()))
   		  usedDeviceIDs.addLast(deviceId.getInstanceNumber());
   	  
   	  /*
   	   * new thread for getting more information about the device
   	   */
   	  new Thread(new Runnable() {
	        @Override
	        public void run() {
		   	  try
		   	  {
		   		  DiscoveryUtils.getExtendedDeviceInformation(gateDevice, remD);
		   		  List<ObjectIdentifier> objects = ((SequenceOf<ObjectIdentifier>) RequestUtils.sendReadPropertyAllowNull(gateDevice, remD, remD.getObjectIdentifier(), PropertyIdentifier.objectList)).getValues();
		   		  
		   		  RootEntity ent = new EntityOfRemoteDevice(PROTOCOL, remD, deviceId, objects, gateDevice);	
		   		  
		      	  /*
		      	   * pass the new device to the gateway as entity
		      	   */
		   		  if (gateway != null)
		      	  {
		      		  gateway.newDevice(PROTOCOL, ent);
		      	  }
		      	  else
		      	  {
		      		  log.info("No Gateway found!");
		      	  }
		   	  }
		   	  catch (BACnetException e)
		   	  {
		   		  log.warning("Gate wasn't able to get all device-information!");
		   		  e.printStackTrace();
		   	  }
	        }
   	  }).start();
     }
     
     /* (non-Javadoc)
      * @see com.serotonin.bacnet4j.event.DeviceEventAdapter#whoIsReceived(com.serotonin.bacnet4j.type.primitive.UnsignedInteger, com.serotonin.bacnet4j.type.primitive.UnsignedInteger)
      */
     @Override
     public void whoIsReceived(UnsignedInteger lowLimit, UnsignedInteger highLimit)
     {
   	  log.info("WhoIs received");
   	  
   	  /*
   	   * forward the whoIs-Request
   	   */
   	  if (gateway != null)
   	  {
   		  gateway.discoverDevice(PROTOCOL, null);
   	  } else
   	  {
   		  log.info("No Gateway found!");
   	  }
     }

		/* (non-Javadoc)
		 * @see com.serotonin.bacnet4j.event.DeviceEventListener#listenerException(java.lang.Throwable)
		 */
		@Override
		public void listenerException(Throwable e)
		{
			e.printStackTrace();
		}
	
		/* (non-Javadoc)
		 * @see com.serotonin.bacnet4j.event.DeviceEventListener#allowPropertyWrite(com.serotonin.bacnet4j.obj.BACnetObject, com.serotonin.bacnet4j.type.constructed.PropertyValue)
		 */
		@Override
		public boolean allowPropertyWrite(BACnetObject obj, PropertyValue pv)
		{
			// TODO Auto-generated method stub
			return false;
		}
	
		/* (non-Javadoc)
		 * @see com.serotonin.bacnet4j.event.DeviceEventListener#propertyWritten(com.serotonin.bacnet4j.obj.BACnetObject, com.serotonin.bacnet4j.type.constructed.PropertyValue)
		 */
		@Override
		public void propertyWritten(BACnetObject obj, PropertyValue pv)
		{
			// TODO Auto-generated method stub
			
		}
	
		/* (non-Javadoc)
		 * @see com.serotonin.bacnet4j.event.DeviceEventListener#iHaveReceived(com.serotonin.bacnet4j.RemoteDevice, com.serotonin.bacnet4j.RemoteObject)
		 */
		@Override
		public void iHaveReceived(final RemoteDevice d, RemoteObject o)
		{
			log.info("IHave received from" + d.toString());
				
			if ( usedDeviceIDs.contains(d.getInstanceNumber()))
			{
				if (gateway != null)
				{
					gateway.newDevice(PROTOCOL, new RootEntity(PROTOCOL, d.getName(), d.getObjectIdentifier()));
				}
				else
				{
					log.info("No Gateway found!");
				}
				
				return;
			}
	   	  
			new Thread(new Runnable() {
			   @Override
			   public void run() {
			   	UnsignedInteger devId = new UnsignedInteger (d.getInstanceNumber());
			   	try
					{
						gateDevice.sendGlobalBroadcast(new WhoIsRequest(devId, devId));
					} catch (BACnetException e)
					{
						log.warning("Gate wasn't able to send WhoIs-Request to find device by deviceId!");
						e.printStackTrace();
					}
			   }
			   	
			}).start();
		}
	
		/* (non-Javadoc)
		 * @see com.serotonin.bacnet4j.event.DeviceEventListener#covNotificationReceived(com.serotonin.bacnet4j.type.primitive.UnsignedInteger, com.serotonin.bacnet4j.RemoteDevice, com.serotonin.bacnet4j.type.primitive.ObjectIdentifier, com.serotonin.bacnet4j.type.primitive.UnsignedInteger, com.serotonin.bacnet4j.type.constructed.SequenceOf)
		 */
		@Override
		public void covNotificationReceived(
				UnsignedInteger subscriberProcessIdentifier,
				RemoteDevice initiatingDevice,
				ObjectIdentifier monitoredObjectIdentifier,
				UnsignedInteger timeRemaining, SequenceOf<PropertyValue> listOfValues)
		{
			COVSubscription subscription = covSubscriptions.get(subscriberProcessIdentifier);
			if (subscription == null)
				return;
			
			int count = listOfValues.getCount();
			int index = 1;
			
			Hashtable<Object, String> notification = new Hashtable<Object, String>();
			while (index <= count)
			{
				PropertyValue propVal = listOfValues.get(index);
//				Object identifier = ParameterTable.bac2gw.get(propVal.getPropertyIdentifier());
//				if(identifier == null)
				Object identifier = propVal.getPropertyIdentifier();
						
				notification.put(identifier, propVal.getValue().toString());
				index++;
			}
			
			gateway.notification(subscription.getPath(), notification);
			
		}
	
		/* (non-Javadoc)
		 * @see com.serotonin.bacnet4j.event.DeviceEventListener#eventNotificationReceived(com.serotonin.bacnet4j.type.primitive.UnsignedInteger, com.serotonin.bacnet4j.RemoteDevice, com.serotonin.bacnet4j.type.primitive.ObjectIdentifier, com.serotonin.bacnet4j.type.constructed.TimeStamp, com.serotonin.bacnet4j.type.primitive.UnsignedInteger, com.serotonin.bacnet4j.type.primitive.UnsignedInteger, com.serotonin.bacnet4j.type.enumerated.EventType, com.serotonin.bacnet4j.type.primitive.CharacterString, com.serotonin.bacnet4j.type.enumerated.NotifyType, com.serotonin.bacnet4j.type.primitive.Boolean, com.serotonin.bacnet4j.type.enumerated.EventState, com.serotonin.bacnet4j.type.enumerated.EventState, com.serotonin.bacnet4j.type.notificationParameters.NotificationParameters)
		 */
		@Override
		public void eventNotificationReceived(UnsignedInteger processIdentifier,
				RemoteDevice initiatingDevice, ObjectIdentifier eventObjectIdentifier,
				TimeStamp timeStamp, UnsignedInteger notificationClass,
				UnsignedInteger priority, EventType eventType,
				CharacterString messageText, NotifyType notifyType,
				Boolean ackRequired, EventState fromState, EventState toState,
				NotificationParameters eventValues)
		{
			// TODO Auto-generated method stub
			
		}
	
		/* (non-Javadoc)
		 * @see com.serotonin.bacnet4j.event.DeviceEventListener#textMessageReceived(com.serotonin.bacnet4j.RemoteDevice, com.serotonin.bacnet4j.type.constructed.Choice, com.serotonin.bacnet4j.type.enumerated.MessagePriority, com.serotonin.bacnet4j.type.primitive.CharacterString)
		 */
		@Override
		public void textMessageReceived(RemoteDevice textMessageSourceDevice,
				Choice messageClass, MessagePriority messagePriority,
				CharacterString message)
		{
			// TODO Auto-generated method stub
			
		}
	
		/* (non-Javadoc)
		 * @see com.serotonin.bacnet4j.event.DeviceEventListener#privateTransferReceived(com.serotonin.bacnet4j.type.primitive.UnsignedInteger, com.serotonin.bacnet4j.type.primitive.UnsignedInteger, com.serotonin.bacnet4j.type.Encodable)
		 */
		@Override
		public void privateTransferReceived(UnsignedInteger vendorId,
				UnsignedInteger serviceNumber, Encodable serviceParameters)
		{
			// TODO Auto-generated method stub
			
		}
	
		/* (non-Javadoc)
		 * @see com.serotonin.bacnet4j.event.DeviceEventListener#reinitializeDevice(com.serotonin.bacnet4j.service.confirmed.ReinitializeDeviceRequest.ReinitializedStateOfDevice)
		 */
		@Override
		public void reinitializeDevice(
				ReinitializedStateOfDevice reinitializedStateOfDevice)
		{
			// TODO Auto-generated method stub
			
		}
	
		/* (non-Javadoc)
		 * @see com.serotonin.bacnet4j.event.DeviceEventListener#synchronizeTime(com.serotonin.bacnet4j.type.constructed.DateTime, boolean)
		 */
		@Override
		public void synchronizeTime(DateTime dateTime, boolean utc)
		{
			// TODO Auto-generated method stub
			
		}
	}
	
	private class COVSubscription
	{
		LinkedList<Object> path;
		long time;
		long duration;
		
		public COVSubscription(LinkedList<Object> path, long time, long duration)
		{
			this.path = path;
			this.time = time;
			this.duration = duration;
		}
		
		public LinkedList<Object> getPath()
		{
			return this.path;
		}
		
		public long getTime()
		{
			return this.time;
		}
		
		public long getDuration()
		{
			return this.duration;
		}
	}
	
	/**
	 * Gets the broadcast address.
	 *
	 * @return the broadcast address
	 * @throws Exception 
	 */
	private String getBroadcastAddress()
	{
		System.setProperty("java.net.preferIPv4Stack", "true");
		NetworkInterface netif = null;
		
		String interfaceName = "eth";
		Integer interfaceNumber = 0;
		
		do
		{
			try
			{
				netif = NetworkInterface.getByName(interfaceName + interfaceNumber);
				log.info("new interface:" + netif.getDisplayName());
			
				InterfaceAddress ifAddr = netif.getInterfaceAddresses().get(0);
				log.info("address:" + ifAddr.getAddress().getHostAddress() 
						+ " name:" + ifAddr.getAddress().getHostName()
						+ " prefix:" + ifAddr.getNetworkPrefixLength()
						+ " broadcast:" + ifAddr.getBroadcast());
			
				String broadcastAddress = ifAddr.getBroadcast().toString();
				return broadcastAddress.substring(1);
			
			} catch (Exception e)
			{
				log.info("Looking for another Interface!\n");
				//e.printStackTrace();
			}
			
			interfaceNumber++;
			
			if (interfaceNumber == 11)
			{
				interfaceName = "en";
				interfaceNumber = 0;
			}
			
		} while ((!interfaceName.contentEquals("en")) && (interfaceNumber <= 10));
		
		return "255.255.255.255";
	}
	
	private void makeLogFile()
	{
		FileHandler fh; 
		
		try {  
	        fh = new FileHandler("BACnetGate.log");  
	        log.addHandler(fh);
	        SimpleFormatter formatter = new SimpleFormatter();  
	        fh.setFormatter(formatter);  

	    } catch (SecurityException | IOException e) {  
	        e.printStackTrace();  
	    }  
	}
}
